/**
gulp-sourcemaps
npm i gulp gulp-jade gulp-stylus autoprefixer-stylus gulp-postcss gulp-uglify --save-dev
список
npm list --depth=0 -g
npm link browser-sync jeet / unlink
 */

var gulp = require('gulp'),
	jade = require('gulp-jade'),
	stylus = require('gulp-stylus'),
	jeet = require('jeet'),
	browserSync = require('browser-sync'),
	fontgen = require('gulp-fontgen'),
    rupture = require('rupture'),
    autoprefixer = require('gulp-autoprefixer');



// ==  TASKS  ==


// JADE
gulp.task('jade-prettyTask', function() {
	gulp.src(['!./src/html/_*.jade',
			  './src/html/*.jade'])
		.pipe(jade({
			pretty: true
		}))
		.pipe(gulp.dest('./build/'));
});


// STYLUS
gulp.task('stylus-prettyTask', function() {
	gulp.src('./src/css/style.styl')
		.pipe(stylus({
					use: [
						jeet(),
						rupture()
					],
					compress: false
		}))
		.pipe(autoprefixer({ browsers: ['last 2 versions'] }))
		.pipe(gulp.dest('./build/css/'));
});


// JS
gulp.task('js-prettyTask', function() {
	gulp.src('./src/js/script.js')
		.pipe(gulp.dest('./build/js/'));
});


// BROWSER-SYNC
gulp.task('browserSyncTask', function() {
	browserSync.init({
					server: {files : ['/html/*.html',
					  				  '/css/*.css',
					  				  '/js/*.js',
					  				  '/fonts/*'], 
							index: "/posts.html",
							baseDir: "./build"
					}
	});
});


// WATCH
gulp.task('watchTask', function() {
		gulp.watch('./src/html/*.jade', ['jade-prettyTask']);
		gulp.watch('./src/css/*.styl', ['stylus-prettyTask']);
		gulp.watch('./src/js/*.js', ['js-prettyTask']);
		gulp.watch('./build/**/*', browserSync.reload);
});

// FONTGEN
gulp.task('fontgen_build', function() {
	gulp.src("./src/fonts/**/*.{ttf,otf}")
		.pipe(fontgen({
				dest: "./build/fonts/"
    }));
});


// =================================

// DEFAULT
gulp.task('default', ['build' ,
					  'watchTask', 
					  'browserSyncTask']
		);
// BUILD
gulp.task('build', ['fontgen_build',
					'js-prettyTask', 
					'stylus-prettyTask' ,
					'jade-prettyTask' 
						]
		);

// to-django
gulp.task('to-django', function(){
	gulp.src("./build/{css,js,fonts}/**/*")
		.pipe(gulp.dest('../backend/django_blog/static/'));
});