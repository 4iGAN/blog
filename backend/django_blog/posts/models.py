from django.db import models

# Create your models here.


class Tag(models.Model):
    name = models.CharField(max_length=64)
    slug = models.SlugField(max_length=128, unique=True)

    def __str__(self):
        return self.name


class Post(models.Model):
    name = models.CharField(max_length=256)
    slug = models.SlugField(max_length=256, unique=True)
    pre_text = models.TextField()
    text = models.TextField()
    date = models.DateField(auto_now=True)
    tags = models.ManyToManyField(Tag)
    published = models.BooleanField(default=False)

    def __str__(self):
        return self.name
