# from django.shortcuts import render
from django.shortcuts import render_to_response
from posts.models import Post


def get_last_posts():
    """
    It function work with DB, get data, transform it
    and return.
    """
    posts_list = Post.objects.prefetch_related('tags').all()[:5]
    result = list(posts_list.values('name',
                                    'slug',
                                    'pre_text',
                                    'date'))

    # pull tags, add to post and transform slug
    for i in range(len(posts_list)):
        tags = posts_list[i].tags.values('name')
        result[i]['tags'] = tags
        result[i]['pre_text'] = result[i]['pre_text'].split('\n')
        result[i]['slug'] = '/post/' + result[i]['slug']

    return result


def last_posts(request):
    """
    Function shows tuple of last posts.
    Function use get_last_posts()
    """
    # for post in posts_list:
    posts = get_last_posts()

    return render_to_response('posts.jade', {'posts_list': posts})


def get_post(slug):
    """
    Function get post by slug and returne it as dict
    """
    result = {}
    post = Post.objects.prefetch_related('tags').get(slug=slug)
    result['name'] = post.name
    result['text'] = post.pre_text.split('\n') + post.text.split('\n')
    result['tags'] = post.tags.values('name')
    result['date'] = post.date

    return result


def post(request, slug):
    """
    Function shows post by request.
    Function use get_post()
    """
    post = get_post(slug=slug)
    return render_to_response('post_one.jade', {'post': post})
