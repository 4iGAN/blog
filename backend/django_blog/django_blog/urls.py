"""django_blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from posts import views as posts
from django.contrib.flatpages import views


urlpatterns = [
    url(r'^admin/', admin.site.urls),

    # posts
    url(r'^$', posts.last_posts),
    url(r'posts/', posts.last_posts, name='posts'),
    url(r'post/(?P<slug>[\w]+)', posts.post, name='post'),

    # flat pages
    url(r'^projects/', views.flatpage, {'url': '/projects/'}, name='projects'),
    url(r'^about/', views.flatpage, {'url': '/about/'}, name='about'),
    url(r'^contacts/', views.flatpage, {'url': '/contacts/'}, name='contacts'),
]
